﻿#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;

struct Waypoint {
	string name;

	Waypoint(string name) {
		this->name = name;
	}
};

class Flight;
struct Airplane {
	friend Flight;

	int type, build_date;

	Airplane(int type, int build_date) {
		this->type = type;
		this->build_date = build_date;
	}

private:
	Airplane() { type = build_date = 0; }
};

struct Passenger {
	enum class Gender { Male, Female } gender;
	vector<string> names;
	string family;
	int birth_date;
	string notes;

	Passenger(vector<string> names, string family, int birth_date, string notes, Passenger::Gender gender) {
		this->names = names;
		this->family = family;
		this->birth_date = birth_date;
		this->notes = notes;
		this->gender = gender;
	}
};

class Flight {
public:
	int num, departure_date, departure_time, travel_time;
	bool international;
	Waypoint *destination;
	Airplane plane;
	vector<Passenger> passengers;
	string ill_criterion;

	Flight(int num, int departure_date, int departure_time, int travel_time, bool international, Waypoint *destination, Airplane plane, vector<Passenger> passengers, string ill_criterion = "java") {
		this->num = num;
		this->departure_date = departure_date;
		this->departure_time = departure_time;
		this->travel_time = travel_time;
		this->international = international;
		this->destination = destination;
		this->plane = plane;
		this->passengers = passengers;
		this->ill_criterion = ill_criterion;
	}

	bool HasMentlHealthProblems(Passenger &passenger) {
		string temp = passenger.notes;
		transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
		if (temp.find(ill_criterion) != -1) {
			return true;
		}
		return false;
	}

	unsigned short GetUnhealthCount() {
		unsigned short count = 0;
		for(Passenger& pass : passengers) {
			count += this->HasMentlHealthProblems(pass);
		}
		return count;
	}
};

struct Airport {
	vector<Flight> flights; 

	Airport(vector<Flight> flights) {
		this->flights = flights;
	}
};

int main() {
	Waypoint dest1("C++ lessons");

	Passenger per1({"Alan", "Torento"}, "Socrento", 13011991, "Java under-coder", Passenger::Gender::Male);
	Passenger per2({ "Inado", "Copri", "Goofy" }, "Fubren", 1041984, "Indo-coder", Passenger::Gender::Male);
	Passenger per3({ "Cobera", "Capani" }, "Doffel", 1041999, "Python enterprise developer", Passenger::Gender::Female);
	Passenger per4({ "Jabba", "Canban" }, "Scrum", 8031975, "Senior java developer", Passenger::Gender::Female);

	Flight flg(10, 15072020, 1530, 87600, true, &dest1, Airplane(4, 16042048), { per1 , per2 , per3, per4 });
	cout << "Mental ills count: " << flg.GetUnhealthCount() << endl;

	Airport port({ flg });



	return 0;
}