﻿#include <iostream>

/* == Demo v2.01 == */

using namespace std;

//Commit: adc07349c488de0402c929b4acef6fa3
template<typename T>
using SquareMatrixPrint = void (*)(const T& item, bool latest);

//Commit: a31034b8ab5977cddc81df9bfd0f971d
template<typename T>
using SquareMatrixInput = T (*)(bool latest);

//Commit: 69eb76c88557a8211cbfc9beda5fc062
template<typename T = int, 
	SquareMatrixPrint<T> print_func = [](const T &item, bool latest) { cout << item << (latest ? '\n' : '\t'); }, 
	SquareMatrixInput<T> input_func = [](bool) { T temp; (cin >> temp); return temp; } >
class SquareMatrix {
	uint8_t grade = 0;
	uint16_t size;
	T *items;
public:
	explicit SquareMatrix(uint8_t grade) {
		if (grade < 2) {
			throw new exception();
		}
		this->grade = grade;
		size = grade * grade;
		items = new T[size]{};
	}
	uint8_t GetGrade() const {
		return grade;
	}
	T *GetPositiveSummArray() const {
		T *sarr = new T[grade]{}, *sarr_c = sarr, temp;
		for (uint8_t i = 0, j; i != grade; ++i) {
			for (j = 0; j != grade; ++j) {
				temp = (*this)[j][i];
				if (temp > 0)
					*sarr_c += temp;
			}
			++sarr_c;
		}
		return sarr;
	}
	void MultiplyByConst(const T& val) {
		for (T *citem = items, *eitem = items + size; citem != eitem; ++citem)
			*citem *= val;
	}
	void Print() const {
		uint8_t sgrade = grade - 1;
		for (uint8_t i = 0, j; i != grade; ++i) {
			for (j = 0; j != sgrade; ++j) {
				print_func((*this)[j][i], false);
			}
			print_func((*this)[j][i], true);
		}
	}
	const T *operator[] (uint8_t i) const {
		return items + i * grade;
	}

	T *operator[] (uint8_t i) {
		return items + i * grade;
	}
	void Input() {
		uint8_t sgrade = grade - 1;
		for (uint8_t i = 0, j; i != grade; ++i) {
			for (j = 0; j != sgrade; ++j) {
				(*this)[j][i] = input_func(false);
			}
			(*this)[j][i] = input_func(true);
		}
	}

	template <typename U, SquareMatrixPrint<U> pf, SquareMatrixInput<U> fi>
	SquareMatrix(const SquareMatrix<U, pf, fi> &sqm) {
		grade = sqm.GetGrade();
		size = grade * grade;
		items = new T[size];
		T *citem = items;
		for (uint8_t i = 0, j; i != grade; ++i) {
			for (j = 0; j != grade; ++j) {
				*citem = sqm[i][j];
				++citem;
			}
		}
	}
	template <typename U, SquareMatrixPrint<U> pf, SquareMatrixInput<U> fi>
	const SquareMatrix<T, print_func, input_func>& operator=(const SquareMatrix<U, pf, fi> &sqm) {
		if (this == &sqm) return *this;
		if (grade != sqm.GetGrade()) {
			grade = sqm.GetGrade();
			size = grade * grade;

			delete[] items;
			items = new T[size];
		}
		T *citem = items;
		for (uint8_t i = 0, j; i != grade; ++i) {
			for (j = 0; j != grade; ++j) {
				*citem = sqm[i][j];
				++citem;
			}
		}
		return *this;
	}
	~SquareMatrix() {
		delete[] items;
	}
};

//Commit: 1c3087f5a5566cec952de71ad0e5b909
void Pfu(const float &item, bool latest) {
	cout << item << (latest ? "\n" : "\t");
}

//Commit: 4571bae333c2e10970bd64775fc8a0e3
float Pfi(bool) {
	string temp;
	cin >> temp;
	return atof(temp.c_str());
}

int main() {
	SquareMatrix<> sq(2);
	cout << "Enter matrix:" << endl;
	sq.Input(); cout << endl;
	sq.Print();

	cout << endl;
	SquareMatrix<float, Pfu, Pfi> gsq(sq);
	gsq.MultiplyByConst(10.3);
	gsq.Print();

	SquareMatrix<float, Pfu, Pfi> fisq = sq;
	cout << "Enter matrix:" << endl;
	fisq.Input(); cout << endl;
	fisq.Print(); cout << endl;

	float *arr = fisq.GetPositiveSummArray();
	for (int i = 0; i != fisq.GetGrade(); ++i) {
		cout << arr[i] << endl;
	}
	return 0;
}