﻿#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Complex {
private:
	double re, im;

public:
	Complex(): re(0), im(0) {
	}

	Complex(double re, double im) : re(re), im(im) {
	}

	Complex(const Complex &arg) : re(arg.re), im(arg.im) {
	}

	Complex operator+(const Complex &arg) const {
		Complex res(re + arg.re, im + arg.im);
		return res;
	}

	Complex operator-(const Complex &arg) const {
		Complex res(re - arg.re, im - arg.im);
		return res;
	}

	Complex operator*(const Complex &arg) const {
		Complex res(re * arg.re - im * arg.im, re * arg.im + arg.re * im);
		return res;
	}

	Complex operator/(const Complex &arg) const {
		double k = arg.re * arg.re + arg.im * arg.im;
		Complex res((re * arg.re + im * arg.im) / k, (re * arg.im - arg.re * im) / k);
		return res;
	}

	Complex operator-() const {
		Complex res(re, -im);
		return res;
	}

	string toString() const {
		stringstream str;
		str << '(' << re << (im >= 0 ? "+" : "") << im << "i)";
		return str.str();
	}

	void getValue(double &re, double &im) const {
		re = this->re;
		im = this->im;
	}

	void setReal(double re) {
		this->re = re;
	}

	void setImaginary(double im) {
		this->im = im;
	}
};

bool operator==(const Complex &arg1, const Complex &arg2) {
	double re1, im1, re2, im2;
	arg1.getValue(re1, im1);
	arg2.getValue(re2, im2);
	if (re1 == re2 && im1 == im2)
		return true;
	return false;
}

ostream &operator<<(ostream &ostr, const Complex &arg) {
	return ostr << arg.toString();
}

istream &operator>>(istream &istr, Complex &arg) {
	double re, im;
	istr >> re >> im;
	arg.setReal(re);
	arg.setImaginary(im);
	return istr;
}

int main() {
	Complex *array1 = new Complex[2];
	cout << "Enter the first complex: ";
	cin >> array1[0];
	cout << "\nEnter the second complex: ";
	cin >> array1[1];

	cout << "\nCheck:\nFirst: " << array1[0] << "\nSecond: " << array1[1];


	cout << "\nAre first ans second equal? : " << (array1[0] == array1[1]);

	cout << "\nConj of first: " << -array1[0];

	cout << "\nSumm of entered: " << array1[0] + array1[1];
	cout << "\nDiff of entered (second - first): " << array1[1] - array1[0];
	cout << "\nMul of entered: " << array1[0] * array1[1];
	cout << "\nQuot of entered (second / first): " << array1[1] / array1[0];

	Complex c1(array1[0]);

	cout << "\nCopy constructor result: " << c1;

	Complex c2;
	c2.setReal(5);
	c2.setImaginary(-10);

	double re, im;
	c2.getValue(re, im);

	cout << "\n\nHand-made assignation result: \nreal = " << re << "\nimaginary = " << im;

	delete[] array1;
	return 0;
}