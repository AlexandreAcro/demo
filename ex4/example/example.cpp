﻿#include <iostream>

using namespace std;

template <class Element>
class Vector {
	int size; Element *array;

public:

	Vector() {
		size = 0;
		array = NULL;
	}

	Vector(int sz) {
		size = sz;
		array = new Element[size];
	}

	~Vector() {
		if (array != NULL)
			delete[] array;
	}

	Element &operator[](int i) {
		return array[i];
	}

	void sort();
	void outputToScreen();

	Vector<Element> &operator=(Vector<Element> &arg2);
	int operator<(Vector<Element> &arg2);
};

template<class Element> void Vector<Element>::sort() {
	for (int i = 0; i < size - 1; i++)
		for (int j = i; j < size - 1; j++)
			if (array[j] > array[j + 1]) {
				Element e = array[j];
				array[j] = array[j + 1];
				array[j + 1] = e;
			}
}

template<class Element> 
void Vector<Element>::outputToScreen() {
	cout << "Array:" << endl;

	for (int i = 0; i < size; i++)
		cout << array[i] << endl;
}

template<class Element>
Vector<Element> &Vector<Element>::operator=(Vector<Element> &arg2) {
	if (array != NULL)
		delete[] array;

	size = arg2.size;
	array = new Element[size];

	for (int i = 0; i < size; i++)
		array[i] = arg2.array[i];

	return *this;
}

template<class Element>
int Vector<Element>::operator<(Vector<Element> &arg2) {
	int isLess = 1;

	int minSize = size < arg2.size ? size : arg2.size;
	for (int i = 0; i < minSize; i++) {
		if ((*this)[i] > arg2[i]) {
			isLess = 0;
			break;
		}
	}
	return isLess;
}

struct Complex {
	double re, im;

	Complex() {
		re = im = 0.0;
	}

	Complex(double num) {
		re = im = num;
	}


	Complex(double x, double y)	{
		re = x;
		im = y;
	}

	Complex& operator=(const Complex &arg2) {
		re = arg2.re;
		im = arg2.im;
		return *this;
	}

	Complex& operator+=(const Complex &arg2) {
		re += arg2.re;
		im += arg2.im;
		return *this;
	}

	Complex operator*(const Complex &arg2) const {
		Complex res;
		res.re = re * arg2.re - im * arg2.im;
		res.im = re * arg2.im + arg2.re * im;
		return res;
	}
};

int operator>(Complex &arg1, Complex &arg2) {
	return (arg1.re * arg1.re + arg1.im * arg1.im > arg2.re * arg2.re + arg2.im * arg2.im);
}

ostream &operator<<(ostream &arg1, const Complex &arg2) {
	return arg1 << "(" << arg2.re << ((arg2.im < 0) ? "" : "+") << arg2.im << "i)";
}

template<typename T>
using SquareMatrixPrint = void (*)(const T &item, bool latest);

template<typename T>
using SquareMatrixInput = T(*)(bool latest);

template<typename T = int,
	SquareMatrixPrint<T> print_func = [](const T &item, bool latest) { cout << item << (latest ? '\n' : '\t'); },
	SquareMatrixInput<T> input_func = [](bool) { T temp; (cin >> temp); return temp; } >
	class SquareMatrix {
	uint8_t grade = 0;
	uint16_t size;
	T *items;

	static unsigned int tcount;
	static void OutputCount(const char *tname, char ndir) {
		cout << "Change of '" << tname << "' is: " << tcount - ndir << "->" << tcount << endl;
	}

	public:
		SquareMatrix(): grade(0), items(nullptr), size(0) {
			++tcount; OutputCount(typeid(*this).name(), 1);
		}

		explicit SquareMatrix(uint8_t grade) {
			if (grade < 2) {
				throw new exception();
			}
			++tcount; OutputCount(typeid(*this).name(), 1);
			this->grade = grade;
			size = grade * grade;
			items = new T[size]{};
		}
		uint8_t GetGrade() const {
			return grade;
		}
		T *GetPositiveSummArray() const {
			if (!grade) throw new exception();
			
			T *sarr = new T[grade]{}, *sarr_c = sarr, temp;
			for (uint8_t i = 0, j; i != grade; ++i) {
				for (j = 0; j != grade; ++j) {
					temp = (*this)[j][i];
					if (temp > 0)
						*sarr_c += temp;
				}
				++sarr_c;
			}
			return sarr;
		}
		void MultiplyByConst(const T &val) {
			if (!grade) throw new exception();

			for (T *citem = items, *eitem = items + size; citem != eitem; ++citem)
				*citem *= val;
		}
		void Print() const {
			if (!grade) throw new exception();

			uint8_t sgrade = grade - 1;
			for (uint8_t i = 0, j; i != grade; ++i) {
				for (j = 0; j != sgrade; ++j) {
					print_func((*this)[j][i], false);
				}
				print_func((*this)[j][i], true);
			}
		}
		const T *operator[] (uint8_t i) const {
			if (!grade) throw new exception();

			return items + i * grade;
		}

		T *operator[] (uint8_t i) {
			if (!grade) throw new exception();

			return items + i * grade;
		}
		void Input() {
			if (!grade) throw new exception();

			uint8_t sgrade = grade - 1;
			for (uint8_t i = 0, j; i != grade; ++i) {
				for (j = 0; j != sgrade; ++j) {
					(*this)[j][i] = input_func(false);
				}
				(*this)[j][i] = input_func(true);
			}
		}

		SquareMatrix(const SquareMatrix &sqm) {
			++tcount; OutputCount(typeid(*this).name(), 1);

			grade = sqm.GetGrade();
			size = grade * grade;
			items = new T[size];
			T *citem = items;
			for (uint8_t i = 0, j; i != grade; ++i) {
				for (j = 0; j != grade; ++j) {
					*citem = sqm[i][j];
					++citem;
				}
			}
		}

	    SquareMatrix& operator=(const SquareMatrix &sqm) {
			if (this == &sqm) return *this;
			if (grade != sqm.GetGrade()) {
				grade = sqm.GetGrade();
				size = grade * grade;

				delete[] items;
				items = new T[size];
			}
			T *citem = items;
			for (uint8_t i = 0, j; i != grade; ++i) {
				for (j = 0; j != grade; ++j) {
					*citem = sqm[i][j];
					++citem;
				}
			}
			return *this;
		}


		~SquareMatrix() {
			--tcount; OutputCount(typeid(*this).name(), -1);

			delete[] items;
		}
};

template <typename T, SquareMatrixPrint<T> pf, SquareMatrixInput<T> fi>
ostream &operator<<(ostream &arg1, const SquareMatrix<T, pf, fi> &arg2) {
	arg2.Print();
	return arg1;
}

template <typename T, SquareMatrixPrint<T> pf, SquareMatrixInput<T> fi>
int operator>(const SquareMatrix<T, pf, fi> &arg1, const SquareMatrix<T, pf, fi> &arg2) {
	T sq1Summ = 0, sq2Summ = 0;
	for (uint8_t grade = 0; grade != arg1.GetGrade(); ++grade)
		sq1Summ += arg1[grade][grade] * arg1[grade][grade];

	for (uint8_t grade = 0; grade != arg2.GetGrade(); ++grade)
		sq2Summ += arg2[grade][grade] * arg2[grade][grade];

	return sq1Summ > sq2Summ;
}

template <typename T, SquareMatrixPrint<T> pf, SquareMatrixInput<T> fi>
unsigned int SquareMatrix<T, pf, fi>::tcount = 0;

void Pfu(const Complex &item, bool latest) {
	cout << item << (latest ? "\n" : "\t");
}

Complex Pfi(bool) {
	string temp;
	Complex cpl;
	cout << "Enter a real part: ";
	cin >> temp;
	cpl.re = atof(temp.c_str());
	cout << "Enter a imaginary part: ";
	cin >> temp;
	cpl.im = atof(temp.c_str());
	return cpl;
}



int main() {
	cout << "Test for template Vector" << endl;
	
	Vector<SquareMatrix<Complex, Pfu, Pfi>> vect1(2);
	SquareMatrix<Complex, Pfu, Pfi> cmat2(2), cmat3(3);
	cmat3.Input();
	cmat2.Input();

	vect1[0] = cmat3;
	vect1[1] = cmat2;

	vect1.outputToScreen();

	vect1.sort();

	cout << "\nSorted array\n";

	vect1.outputToScreen();

	SquareMatrix<> tmat(2);

	return 0;
}