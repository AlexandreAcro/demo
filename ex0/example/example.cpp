﻿#include <iostream>
#include <Windows.h>
#include <windowsx.h>
#include <math.h>
#include <tchar.h>

#define PI 3.14159265
#define GetStockPen(i) ((HPEN)GetStockObject(i))

using namespace std;
namespace nonvt {
	class Figure {
	protected:
		HWND hWnd;
		HDC hDC;
		COLORREF Color;
		int CenterX;
		int CenterY;
	public:
		Figure(HWND _hWnd, int iCenterX, int iCenterY) {
			cout << "Figure: constructor" << endl;
			hWnd = _hWnd;
			hDC = GetDC(_hWnd);
			Color = RGB(255, 0, 0);
			CenterX = iCenterX;
			CenterY = iCenterY;
		}
		virtual ~Figure(void) {
			ReleaseDC(hWnd, hDC);
			cout << "Figure: destructor" << endl;
		}
		void SetNewColor(COLORREF NewColor) {
			Hide();
			Color = NewColor;
			Show();
		}
		virtual void Show() = 0;
		virtual void Hide() {
			COLORREF prev_col = Color;
			Color = RGB(0, 0, 0);
			Show();
			Color = prev_col;
		}
		void Move(int DeltaX, int DeltaY) {
			Hide();
			CenterX += DeltaX;
			CenterY += DeltaY;
			Show();
		}
	};
	class Rectangle_my : public Figure {
		int h;
		int w;
	public: 
		Rectangle_my(HWND _hWnd, int iCenterX, int iCenterY, int ih, int iw) : Figure(_hWnd, iCenterX, iCenterY) {
			cout << "Rectangle_my: constructor" << endl;
			h = ih;
			w = iw;
		}
		void Show() {
			HPEN hPen = CreatePen(PS_SOLID, 1, Color);
			HBRUSH hBrush = GetStockBrush(NULL_BRUSH);
			SelectObject(hDC, hPen);
			SelectObject(hDC, hBrush);
			Rectangle(hDC, CenterX, CenterY, CenterX + w, CenterY + h);
			DeleteObject(hBrush);
			DeleteObject(hPen);
		}
		~Rectangle_my() {
			cout << "Rectangle_my: destructor" << endl;
		}
	};
	class Flower : public Figure {
		int width, height;
		HBRUSH brush_static;

		inline void DrawEllipse(int left, int top, int width, int height) {
			Ellipse(hDC, left, top, left + width, top + height);
		}
	public:
		Flower(HWND _hWnd, int iCenterX, int iCenterY, int width, int height) : Figure(_hWnd, iCenterX, iCenterY), width(width), height(height) {
			cout << "Flower: constructor" << endl;
			brush_static = CreateSolidBrush(RGB(255, 255, 0));
		}

		virtual void Show() {
			HBRUSH brush = CreateSolidBrush(Color);
			int lefthalf = width / 2, tophalf = height / 2,
				width_thr = width / 3, height_thr = height / 3;
			lefthalf += CenterX - width_thr / 2; tophalf += CenterY - height_thr / 2;
			SelectObject(hDC, GetStockPen(BLACK_PEN));
			SelectObject(hDC, brush);
			DrawEllipse(CenterX, CenterY + height_thr, width_thr, height_thr);
			DrawEllipse(CenterX + width_thr, CenterY, width_thr, height_thr);
			DrawEllipse(CenterX + width_thr * 2, CenterY + height_thr, width_thr, height_thr);
			DrawEllipse(CenterX + width_thr, CenterY + height_thr * 2, width_thr, height_thr);

			int width_thr_sin = width_thr * sin(PI / 4), height_thr_sin = height_thr * sin(PI / 4);
			DrawEllipse(lefthalf - width_thr_sin, tophalf - height_thr_sin, width_thr, height_thr);
			DrawEllipse(lefthalf + width_thr_sin, tophalf - height_thr_sin, width_thr, height_thr);
			DrawEllipse(lefthalf - width_thr_sin, tophalf + height_thr_sin, width_thr, height_thr);
			DrawEllipse(lefthalf + width_thr_sin, tophalf + height_thr_sin, width_thr, height_thr);
			SelectObject(hDC, brush_static);
			DrawEllipse(CenterX + width_thr / 2, CenterY + height_thr / 2, width_thr * 2, height_thr * 2);
			DeleteObject(brush);
		}
		virtual void Hide() {
			COLORREF prev_col = Color;
			SelectObject(hDC, GetStockBrush(BLACK_BRUSH));
			int left = CenterX - width / 2, top = CenterY - height / 2;
			Rectangle(hDC, left, top, left + width, top + height);
			Color = prev_col;
		}
		virtual ~Flower() {
			DeleteObject(brush_static);
			cout << "Flower: destructor" << endl;
		}
	};
	class FlowerInRectangle : public Flower, public Rectangle_my {
	public:
		FlowerInRectangle(HWND _hWnd, int iCenterX, int iCenterY, int width, int height) :Flower(_hWnd, iCenterX, iCenterY, width, height), Rectangle_my(_hWnd, iCenterX, iCenterY, height, width) {
			cout << "FlowerInRectangle: constructor" << endl;
		}
		void Show() {
			Rectangle_my::Show();
			Flower::Show();
		}
		void Hide() {
			Flower::Hide();
		}
		virtual ~FlowerInRectangle() {
			cout << "FlowerInRectangle: destructor" << endl;
		}
	};
}
namespace vt {
	class Figure {
	protected:
		HWND hWnd;
		HDC hDC;
		COLORREF Color;
		int CenterX;
		int CenterY;
	public:
		Figure(HWND _hWnd, int iCenterX, int iCenterY) {
			cout << "Figure: constructor" << endl;
			hWnd = _hWnd;
			hDC = GetDC(_hWnd);
			Color = RGB(255, 0, 0);
			CenterX = iCenterX;
			CenterY = iCenterY;
		}
		virtual ~Figure(void) {
			ReleaseDC(hWnd, hDC);
			cout << "Figure: destructor" << endl;
		}
		void SetNewColor(COLORREF NewColor) {
			Hide();
			Color = NewColor;
			Show();
		}
		virtual void Show() = 0;
		virtual void Hide() {
			COLORREF prev_col = Color;
			Color = RGB(0, 0, 0);
			Show();
			Color = prev_col;
		}
		void Move(int DeltaX, int DeltaY) {
			Hide();
			CenterX += DeltaX;
			CenterY += DeltaY;
			Show();
		}
	};
	class Rectangle_my : public virtual Figure {
		int h;
		int w;
	public:
		Rectangle_my(HWND _hWnd, int iCenterX, int iCenterY, int ih, int iw) : Figure(_hWnd, iCenterX, iCenterY) {
			cout << "Rectangle_my: constructor" << endl;
			h = ih;
			w = iw;
		}
		void Show() {
			HPEN hPen = CreatePen(PS_SOLID, 1, Color);
			HBRUSH hBrush = GetStockBrush(NULL_BRUSH);
			SelectObject(hDC, hPen);
			SelectObject(hDC, hBrush);
			Rectangle(hDC, CenterX, CenterY, CenterX + w, CenterY + h);
			DeleteObject(hBrush);
			DeleteObject(hPen);
		}
		~Rectangle_my() {
			cout << "Rectangle_my: destructor" << endl;
		}
	};
	class Flower : public virtual Figure {
		int width, height;
		HBRUSH brush_static;

		inline void DrawEllipse(int left, int top, int width, int height) {
			Ellipse(hDC, left, top, left + width, top + height);
		}
	public:
		Flower(HWND _hWnd, int iCenterX, int iCenterY, int width, int height) : Figure(_hWnd, iCenterX, iCenterY), width(width), height(height) {
			cout << "Flower: constructor" << endl;
			brush_static = CreateSolidBrush(RGB(255, 255, 0));
		}
		virtual void Show() {
			HBRUSH brush = CreateSolidBrush(Color);
			int lefthalf = width / 2, tophalf = height / 2,
				width_thr = width / 3, height_thr = height / 3;

			lefthalf += CenterX - width_thr / 2; tophalf += CenterY - height_thr / 2;
			SelectObject(hDC, GetStockPen(BLACK_PEN));
			SelectObject(hDC, brush);
			DrawEllipse(CenterX, CenterY + height_thr, width_thr, height_thr);
			DrawEllipse(CenterX + width_thr, CenterY, width_thr, height_thr);
			DrawEllipse(CenterX + width_thr * 2, CenterY + height_thr, width_thr, height_thr);
			DrawEllipse(CenterX + width_thr, CenterY + height_thr * 2, width_thr, height_thr);
			int width_thr_sin = width_thr * sin(PI / 4), height_thr_sin = height_thr * sin(PI / 4);
			DrawEllipse(lefthalf - width_thr_sin, tophalf - height_thr_sin, width_thr, height_thr);
			DrawEllipse(lefthalf + width_thr_sin, tophalf - height_thr_sin, width_thr, height_thr);
			DrawEllipse(lefthalf - width_thr_sin, tophalf + height_thr_sin, width_thr, height_thr);
			DrawEllipse(lefthalf + width_thr_sin, tophalf + height_thr_sin, width_thr, height_thr);
			SelectObject(hDC, brush_static);
			DrawEllipse(CenterX + width_thr / 2, CenterY + height_thr / 2, width_thr * 2, height_thr * 2);
			DeleteObject(brush);
		}
		virtual void Hide() {
			COLORREF prev_col = Color;
			SelectObject(hDC, GetStockBrush(BLACK_BRUSH));
			int left = CenterX - width / 2, top = CenterY - height / 2;
			Rectangle(hDC, left, top, left + width, top + height);
			Color = prev_col;
		}
		virtual ~Flower() {
			DeleteObject(brush_static);
			cout << "Flower: destructor" << endl;
		}
	};

	class FlowerInRectangle : public Flower, public Rectangle_my {
	public:
		FlowerInRectangle(HWND _hWnd, int iCenterX, int iCenterY, int width, int height) :Flower(_hWnd, iCenterX, iCenterY, width, height), Rectangle_my(_hWnd, iCenterX, iCenterY, height, width), Figure(_hWnd, iCenterX, iCenterY) {
			cout << "FlowerInRectangle: constructor" << endl;
		}

		void Show() {
			Rectangle_my::Show();
			Flower::Show();
		}
		void Hide() {
			Flower::Hide();
		}
		virtual ~FlowerInRectangle() {
			cout << "FlowerInRectangle: destructor" << endl;
		}
	};
}
int main() {
	cout << "Non-virtal inheritance:" << endl << endl;
	HWND hWnd = GetConsoleWindow();
	{
		 nonvt::FlowerInRectangle flwrect(hWnd, 700, 50, 100, 100);
		 flwrect.Show();
	}
	cout << endl << endl << "Virtual inheritance:" << endl << endl;
	{
		vt::FlowerInRectangle flwrect(hWnd, 750, 200, 150, 100);
		flwrect.Show();
	}
	return 0;
}